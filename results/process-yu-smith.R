## -*- electric-pair-mode: 1 -*-
## process-yu-smith.R
## Get statistics and graphs from Yu & Smith (2007) experiments
setwd("/home/rberaldo/Documents/unicamp/codigo/experimento-ciel-vii/results/")

library(ggplot2)
library(dplyr)
theme_set(theme_minimal())

## For Yu and Smith, 2007, I ran 100 iterations of each condition. This
## function gets an average of lexicon size and number of words converged
## per condition.
get_average_learning <- function(experiment) {
    # idea: add standard deviation to each of the observations
    amb_levels = unique(experiment$AMBIGUITY)
    lex_matrix <- matrix(NA, ncol = 6)
    for (amb in amb_levels) {
        current_experiment <- experiment%>%filter(AMBIGUITY == amb)
        n_runs <- length(unique(current_experiment$RUN))
        for (run in 1:n_runs) {
            current_data <- current_experiment%>%filter(RUN == run)
            lex_matrix <- rbind(lex_matrix, c(run,
                                              mean(current_data$LEX_SIZE),
                                              mean(current_data$NO_CONVERGED),
                                              unique(current_data$AMBIGUITY),
                                              unique(current_data$FORGETFUL),
                                              unique(current_data$FORGET_AFTER)))
        }
    }
    lex_by_run <- as.data.frame(lex_matrix)
    colnames(lex_by_run) <- c("run", "lex_size", "no_converged", "ambiguity",
                              "forgetful", "forget_after")
    return(lex_by_run[-1,]) # hack to remove first row of NA results
}

## Make plots
make_plot <- function (data, title) {
    ggplot(data, aes(run)) +
        geom_line(aes(y = lex_size, linetype = "Encountered Words")) +
        geom_line(aes(y = no_converged, linetype = "Converged Words")) +
        scale_linetype_discrete() +
        ggtitle(title) +
        xlab("Trial") + xlim(1, length(unique(data$run))) +
        ylab("Size of Lexicon") + ylim(0, max(data$lex_size + 2)) +
        theme(legend.position = c(.65, .35), legend.title=element_blank()) +
        facet_wrap(~ ambiguity, ncol = 2, labeller = label_both)
}


## Make plots
make_plot_yu2 <- function (data, title) {
    ggplot(data, aes(run)) +
        geom_line(aes(y = lex_size, linetype = "Encountered Words")) +
        geom_line(aes(y = no_converged, linetype = "Converged Words")) +
        scale_linetype_discrete() +
        ggtitle(title) +
        xlab("Trial") + xlim(1, length(unique(data$run))) +
        ylab("Size of Lexicon") + ylim(0, max(data$lex_size + 2)) +
        theme(legend.position = c(.8, .1), legend.title=element_blank()) +
        facet_wrap(~ forget_after, ncol = 2, labeller = label_both)
}

##
## Experimental results
## Lexicon size and converged words are collapsed by run, since I ran each
## experiment 100 times; thus I want the average of each

## Processing Yu & Smith (2007) experiment 1
## Three conditions of ambiguity: 2, 3 and 4
## Lexicon == 12 items
## Word exposures == 6
## Forgetfullness == 0
yu1 <- read.csv("yu1.csv")
yu1_forget0 <- get_average_learning(yu1%>%filter(FORGETFUL == "False"))
title = "Yu & Smith (2007), experiment 1"
filename = "yu1"

yu1_plot <- make_plot(yu1_forget0, title)

ggsave(paste(filename,
             "-forget", 0,
             ".png",
             sep = ""), plot = yu1_plot)

## Processing experiment 1 with forgetfullness
yu1_forget <- yu1%>%filter(FORGETFUL == "True")%>%droplevels()
for (f in unique(yu1_forget$FORGET_AFTER)) {
    exp <- get_average_learning(yu1_forget%>%filter(FORGET_AFTER == f))

    plot <- make_plot(exp, paste(title, ",", " forgetfulness of ", f, sep = ""))

    ggsave(paste(filename,
                 "-forget", f,
                 ".png",
                 sep = ""), plot = plot)
}

## Getting the proportion of words learned depending on forget_after
correct_proportions <- function(data, vocab_size) {
    proportions_matrix <- matrix(NA, nrow = length(unique(data$ambiguity)), ncol = 3)
    i <- 1
    for (a in unique(data$ambiguity)) {
        current_data <- tail(data%>%filter(ambiguity == a), 1)
        proportions_matrix[i,] <- c(a,
                                    current_data$no_converged/vocab_size * 100,
                                    current_data$forget_after)
        i = i + 1
    }
    return(proportions_matrix)
}

correct_proportions_across_forgetfulness <- function(data, vocab_size) {
    proportions <- matrix(NA, nrow = length(unique(data$AMBIGUITY)), ncol = 3)
    for (f in unique(data$FORGET_AFTER)) {
        m <- correct_proportions(get_average_learning(data%>%filter(FORGET_AFTER == f)),
                                 vocab_size)
        proportions <- rbind(proportions, m)
    }
    proportions <- as.data.frame(proportions) # hack
    colnames(proportions) <- c("ambiguity", "prop_learned", "forget_after")
    return(na.omit(proportions))
}

## Plotting proportion of words learned as a function of forgetfulness
proportions_plot <- ggplot(correct_proportions_across_forgetfulness(yu1, 18),
       aes(x = ambiguity, y = prop_learned)) +
    geom_bar(stat = "identity") +
    xlab("Ambiguity") +
    ylab("Proportion learned (%)") +
    facet_wrap(~ forget_after, labeller = label_both)

ggsave("yu1_proportion_learned.png", plot = proportions_plot)

## Comparing to other models
reported_model_results <- read.csv("prop-correct.csv")
plot <- ggplot(reported_model_results, aes(x = condition, y = prop_learned * 100)) +
    geom_bar(stat = "identity") +
    xlab("Condition") +
    ylab("Proportion learned (%)") +
    facet_wrap(~ model)

ggsave("yu1-model-comparsion.png", plot = plot)

## I want to join the average learning into one dataframe I can facet wrap.
## I have to first step through each forget_after value and then rbind to a
## new data frame.
join_average_learning <- function(data) {
    average_learning <- data.frame()
    for (f in unique(data$FORGET_AFTER)) {
        average_learning <- rbind(average_learning, get_average_learning(data%>%filter(FORGET_AFTER == f)))
    }
    return(average_learning)
}

## Processing Yu & Smith (2007), experiment 2.1
## Lexicon == 9 items
## Word exposures == 8
## Ambiguity == 4
## Forgetfullness == 0
yu2_1 <- read.csv("yu2_1.csv")
yu2_1_forget0 <- get_average_learning(yu2_1%>%filter(FORGETFUL == "False"))
title = "Yu and Smith (2007), experiment 2.1"
filename = "yu2_1"

yu2_1_plot <-make_plot(yu2_1_forget0, title)

ggsave(paste(filename,
             "-forget", 0,
             ".png",
             sep = ""), plot = yu2_1_plot)

## Processing experiment 2.1 with forgetfullness
yu2_1_forget <- yu2_1%>%filter(FORGETFUL == "True")%>%droplevels()

plot <- make_plot_yu2(join_average_learning(yu2_1_forget),
                      paste(title, "with forgetfulness"))

ggsave(paste(filename,
             "-forgetful",
             ".png",
             sep = ""), plot = plot)

## Processing Yu & Smith (2007), experiment 2.2
## Lexicon == 9 items
## Word exposures == 12
## Ambiguity == 4
## Forgetfullness == 0
yu2_2 <- read.csv("yu2_2.csv")
yu2_2_forget0 <- get_average_learning(yu2_2%>%filter(FORGETFUL == "False"))
title = "Yu and Smith (2007), experiment 2.2"
filename = "yu2_2"

yu2_2_plot <-make_plot(yu2_2_forget0, title)

ggsave(paste(filename,
             "-forget", 0,
             ".png",
             sep = ""), plot = yu2_2_plot)

## Processing experiment 2.2 with forgetfullness
yu2_2_forget <- yu2_2%>%filter(FORGETFUL == "True")%>%droplevels()

plot <- make_plot_yu2(join_average_learning(yu2_2_forget),
                      paste(title, "with forgetfulness"))

ggsave(paste(filename,
             "-forgetful",
             ".png",
             sep = ""), plot = plot)

## Processing Yu & Smith (2007), experiment 2.3
## Lexicon == 18 items
## Word exposures == 6
## Ambiguity == 4
## Forgetfullness == 0
yu2_3 <- read.csv("yu2_3.csv")
yu2_3_forget0 <- get_average_learning(yu2_3%>%filter(FORGETFUL == "False"))
title = "Yu and Smith (2007), experiment 2.3"
filename = "yu2_3"

yu2_3_plot <-make_plot(yu2_3_forget0, title)

ggsave(paste(filename,
             "-forget", 0,
             ".png",
             sep = ""), plot = yu2_3_plot)

## Processing experiment 2.3 with forgetfullness
yu2_3_forget <- yu2_3%>%filter(FORGETFUL == "True")%>%droplevels()

plot <- make_plot_yu2(join_average_learning(yu2_3_forget),
                      paste(title, "with forgetfulness"))

ggsave(paste(filename,
             "-forgetful",
             ".png",
             sep = ""), plot = plot)
