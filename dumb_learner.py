#!/usr/bin/env python
# coding: utf-8
# dumb-learner.py
# Dumb Word Learner
# Part of yu-smith-2007.py
#
# This toy example simply counts the number of occurrences of
# word-referent; winner is defined as referent which has the strongest
# association score

class Hypothesis:
    def __init__(self, word):
        self.word = word
        self.refs = Counter()
    def __repr__(self):
        return self.word
    def add_ref(self, observation):
        self.refs[observation] += 1
    def winner(self):
        return self.refs.most_common(3)

def word_exists (word, list):
    words = {item.word for item in list}
    if word in words:
        return True
    else:
        return False

def find_hypothesis (word, list):
    i = 0
    for w in list:
        if w.word == word:
            return i
        i += 1

def push_word (word, list):
    list.append(Hypothesis(word))

def print_report(report, run_number):
    print("REPORT FOR RUN NUMBER ", run_number + 1)
    print(report)
    print("\n")


def dumb_learner (dataset):
    "counts word-ref coocorrences"
    hypotheses = []

    run = 0
    for trial in dataset:
        report = '' # zero the report string
        trial[0] = words
        trial[1] = referents

        for word in words:
            if not word_exists(word, hypotheses):
                push_word(word, hypotheses)
            # we now need to get the current hypothesis index so we can
            # access the observed referents
            hypothesis_index = find_hypothesis(word, hypotheses)
            for ref in referents:
                hypotheses[hypothesis_index].add_ref(ref)

        # reporting
        for w in hypotheses:
            report = report + str(w.word) + ": " + str(w.winner()) + "\n"
        print_report(report, run)
        run += 1

    return hypotheses
