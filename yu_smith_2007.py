#!/usr/bin/env python
# coding: utf-8
# yu_smith_2007.py
# Implements Yu & Smith’s (2007) experiment

# using 54 nouns from Marques et al., 2007 just for fun
# see https://link.springer.com/article/10.3758/BF03193013
# and words.txt

import random
from collections import Counter
import vocab_generator

WORD_FILE = "words/words.txt"

# Experiment generator
# A trial consists of showing n words, where n = ambiguity
# An experiment is a set of trials one after the other

def gen_trial (indices, vocabulary):
    "builds a trial, returning a list with words and referents"
    pairs = []
    for i in indices:
        pairs.append((vocabulary[i].sound, vocabulary[i].semantics))

    words = [e[0] for e in pairs]
    refs = [e[1] for e in pairs]

    # ugly albeit ineviable, according to Miky
    random.shuffle(words)
    random.shuffle(refs)

    return [words,refs]

def gen_trial_orders (ambiguity, vocabulary, max_word_exposures):
    "generates word orders for trials"
    word_indices = list(range(0, len(vocabulary))) * max_word_exposures
    word_indices_recover = word_indices.copy()
    trial_orders = []

    limit = 0
    while word_indices:
        # ambiguity means how many words per trial
        trial = random.sample(word_indices, ambiguity)
        # check for repeated indices
        if len(set(trial)) == ambiguity:
            trial_orders.append(trial)
            for element in trial:
                word_indices.remove(element)
        # hack to guarantee we don’t get into an infinite loop
        limit += 1
        if limit > 1000:
            print("Cleaning trial_orders")
            trial_orders = []
            print("Resetting word_indices")
            word_indices = word_indices_recover.copy()
            limit = 0

    return trial_orders

def gen_experiment_data (ambiguity, vocabulary, max_word_exposures):
    "outputs word-referent pairings"
    trial_orders = gen_trial_orders(ambiguity, vocabulary, max_word_exposures)
    trials = []

    for trial in trial_orders:
        trials.append(gen_trial(trial, vocabulary))

    return trials

# bugs bugs bugs: debugging functions
def occur_count (experiment, word):
    "check how many times each word is used"
    count = 0

    for trial in range(0, len(e)):
        if e[trial][0].count(word):
            count += 1

    return count
