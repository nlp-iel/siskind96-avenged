#!/usr/bin/env python
# coding: utf-8
# vocab_generator.py
# Gets words from txt file

class Word:
    "words from a shared vocabulary used to generate stimuli"
    def __init__(self, sound, semantics):
        self.sound = sound
        self.semantics = semantics
    def __repr__(self):
        return self.sound

def gen_vocabulary (f, size):
    "reads size lines from file to generate Word objects"
    with open(f, "r") as txtfile:
        lines = txtfile.readlines()
        words = []
        for i in range(size):
            word = lines[i][:-1] # remove new lines
            words.append(Word(word, word.upper()))

        return words
