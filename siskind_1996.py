#!/usr/bin/env python
# coding: utf-8
# siskind_1996.py
# Implements a word learner à la Siskind (1996)

import time, csv, sys, os, random
import vocab_generator
import yu_smith_2007
import trueswell_et_al_2013

STATS_DIR = "results/"

# LEXICAL ITEMS
# each word has three variables:
# - one set for possible meanings P(w)
# - one set for necessary meanings N(w)
# - one bool state for frozen words P(w) = N(w)
# I’m using sets for comparing the meanings more easily and independend of order

class LexicalItem:
    "lexical item with P(w), N(w) and freeze state"
    def __init__(self, word, conceptual_symbols):
        self.word = word
        self.possible_meanings = {s for s in conceptual_symbols}
        self.necessary_meanings = set()
        self.frozen = False
    def __repr__(self):
        return self.word
    def freeze(self):
        "freezes item, stopping changes to it"
        self.frozen = True
        print("Word", self, "has converged on its meaning")
    def has_converged(self):
        "check if word has converged"
        if self.possible_meanings == self.necessary_meanings:
            return True
        return False
    def remove_p_meaning(self, conceptual_symbol):
        "removes meaning from P(w)"
        if not self.frozen:
            self.possible_meanings.remove(conceptual_symbol)
            if self.has_converged():
                self.freeze()
    def add_n_meaning(self, conceptual_symbol):
        "adds meaning to N(w), freezing when converged"
        if not self.frozen and conceptual_symbol in self.possible_meanings:
            self.necessary_meanings.add(conceptual_symbol)
            if self.has_converged():
                self.freeze()
        else:
            print("Error: Trying to add N meaning not in P(w) ", conceptual_symbol)

def view_lexicon(lexicon):
    "convenient way to check the lexicon"
    print("LEXICON (" + str(len(lexicon)) + " items)\n")
    for word in lexicon:
        print('{} → N(w): {} / P(w): {}'.format(word.word,
                                                str(word.necessary_meanings),
                                                str(word.possible_meanings)))
    print()

    # print frozen meanings
    frozen_words = [w for w in lexicon if w.frozen]
    print(str(len(frozen_words)), "frozen meanings:", frozen_words)

def word_exists (word, lexicon):
    "checks if word exists in lexicon"
    if {w for w in lexicon if w.word == word}:
        return True
    return False

def add_word (word, conceptual_symbols, lexicon):
    "add word to lexicon and initialize its P(w)"
    if not word_exists(word, lexicon):
        lexicon.append(LexicalItem(word, conceptual_symbols))

def add_new_words (words, conceptual_symbols, lexicon):
    "add newly observed words to the lexicon"
    for word in words:
        if not word_exists(word, lexicon):
            print("Initializing word", word, "with P(w)", conceptual_symbols)
            add_word(word, conceptual_symbols, lexicon)

# remember each utterance meaning (which is word meaning) is atomic, so there’s
# not intersection between all the m generated in these experiments… Unless if I
# were using features… Could some of the behavior seen in the other studies be
# captured by using features? Should this be explored for Koehne et al. 2013?

def get_words (pair):
    "receives a list and returns words, conventionally stored at index 0"
    return pair[0]

def get_meanings (pair):
    "receives a list [[WORDS],[MEANING1]…[MEANINGn]] and returns meanings"
    # check for only one meaning
    if len(pair) - 1 == 1:
        return [pair[1]]
    else:
        return pair[1:]

def get_all_word_meanings (words, lexicon, kind):
    "gets all P(w) or N(w) for words and returns a set"
    meanings = []
    if kind == "p":
        for w in words:
            meanings = meanings + [i.possible_meanings for i in lexicon if i.word == w]
        return set.union(*meanings)
    if kind == "n":
        for w in words:
            meanings = meanings + [i.necessary_meanings for i in lexicon if i.word == w]
        return set.union(*meanings)

def get_all_utterance_meanings (meanings):
    "gets all concepts in utterance meanings and returns a set"
    utterance_meanings = set()
    for item in meanings:
        utterance_meanings.update(item)

    return utterance_meanings

def get_unique_utterance_meanings (meanings):
    "utterance meanings which occur only once accross all meanings"
    # meanings will always be inside lists, so we need to flatten them
    flattened_meanings = [m for i in meanings for m in i]
    unique_meanings = set()

    i = 0
    while i < len(flattened_meanings):
        for m in flattened_meanings:
            if m not in flattened_meanings[i+1:]:
                unique_meanings.add(m)
            i += 1

    return unique_meanings

def get_common_utterance_meanings (meanings):
    "get meanings which occur across all utterance meanings"
    common_meanings = set(meanings[0])
    for meaning in meanings[1:]:
        common_meanings.intersection_update(meaning)

    return common_meanings

# THE HEURISTICS
# For now we’re ignoring homonymy and noise; necessary Koehne et al’s
# (2013) experiment, apparently

# step 1: reduce referential uncertainty
# this is done in two ways:
# - remove utterance meaning m if some N(w) for some word w is missing in that
# utternace (m must be derived from the meanings of the words)
# if {all in N(w)} - {all in m}
# - remove m if it contains meanings not in P(w) of any words w in the utterance
# {all in m} - {all in P(w)}
# I must consider the universal set as well

def rule1 (pair, lexicon):
    "reduces referential uncertainty by removing inconsistent meanings"
    print("RULE 1")

    utterance_words = get_words(pair)
    utterance_meanings = get_meanings(pair)
    surviving_utterance_meanings = utterance_meanings.copy()
    all_possible_word_meanings = get_all_word_meanings(utterance_words, lexicon, "p")
    all_necessary_word_meanings = get_all_word_meanings(utterance_words, lexicon, "n")

    for m in utterance_meanings:
        # print("\nprocessing meaning", m)
        # check for missing necessary meanings…
        if all_necessary_word_meanings - set(m):
            print("Necessary removing meaning:", m)
            surviving_utterance_meanings.remove(m)
        # …and for missing possible meanings
        elif set(m) - all_possible_word_meanings:
            print("Possible removing meaning:", m)
            surviving_utterance_meanings.remove(m)

    # rebuild pair, now with less referential uncertainty
    return [utterance_words] + surviving_utterance_meanings

# step 2: reduce P(w)
# remove from P(w) conceptual symbols which do not appear in some remaining
# utterance meaning (in other words, what does the current set of m say about
# the possible meanings of the words?)
# {all in P(w)} - {all in m}

def rule2 (pair, lexicon):
    "reduce P(w) for all lexical items"
    print("RULE 2")

    utterance_words = get_words(pair)
    all_possible_word_meanings = get_all_word_meanings(utterance_words, lexicon, "p")
    all_utterance_meanings = get_all_utterance_meanings(get_meanings(pair))
    meanings_to_remove = all_possible_word_meanings - all_utterance_meanings

    for m in meanings_to_remove:
        for word in [i for i in lexicon if i.word in utterance_words]:
            if m in word.possible_meanings and not word.frozen:
                print("Removing meaning", m, "from lexical item", word)
                word.remove_p_meaning(m)

# step 3: add to N(w)
# add to N(w) any conceptual symbols that appear in every reamining utterance
# meaning but that are missing from P(w’) for every other word symbol w in the
# utterance (what can each word uniquely contribute that must be part of the
# meaning?)

def rule3 (pair, lexicon):
    "adds meaning from P(w) to N(w) when only w can possibly contribute the meaning"
    print("RULE 3")

    utterance_words = get_words(pair)
    common_utterance_meanings = get_common_utterance_meanings(get_meanings(pair))

    # iterate over lexicon words which are in the utterance
    for word in [i for i in lexicon if i.word in utterance_words]:
        # first, check if we only have one word
        if len(utterance_words) == 1:
            # in this case, we need only check if a possible meaning is in
            # every utterance meaning
            for meaning in get_all_word_meanings([str(word)], lexicon, "p"):
                if meaning in common_utterance_meanings and not word.frozen:
                    print("Adding meaning", meaning, "to N(w) of", word)
                    word.add_n_meaning(meaning)
        else:
            # make a list of other words
            other_words = utterance_words.copy()
            other_words.remove(str(word))
            # for each possible meaning of word (get_all_word_meanings accepts
            # a list of strings, thus the hack below)
            for meaning in get_all_word_meanings([str(word)], lexicon, "p"):
                # is this meaning is exclusive of this word AND present in all
                # utterance meangs
                if (meaning not in get_all_word_meanings(other_words, lexicon, "p") and
                    meaning in common_utterance_meanings and
                    not word.frozen):
                    print("Adding meaning", meaning, "to N(w) of", word)
                    word.add_n_meaning(meaning)

# step 4: reduce P(w) again
# for each w, remove for P(w) any conceptual symbols that appear only once in
# every remaining utterance meaning if they are in N(w’) for some other symbol
# w’ in the utterance (that is, what must other words necessarily contribute
# which I can remove from possible meanings of the current word?)

def rule4 (pair, lexicon):
    "remove from P(w) if meaning appears only once and is in N(w')"
    print("RULE 4")

    utterance_words = get_words(pair)
    unique_utterance_meanings = get_unique_utterance_meanings(get_meanings(pair))

    # check for single word utterance; since we depend on the meanings
    # contributed by other words, we can skip this step when we have only
    # one word
    if len(utterance_words) == 1:
        return "Only one word in utterance"

    for word in [i for i in lexicon if i.word in utterance_words]:
        # remove from P(w) if it appears only once in the utterance
        # and if some other word contributes it necessarily
        # make a list of other words
        other_words = utterance_words.copy()
        other_words.remove(str(word))
        # for each possible meaning of word
        for meaning in get_all_word_meanings([str(word)], lexicon, "p"):
            # check all conditions: meaning is among the unique ones, some
            # other word contributes it necessarily and word is frozen
            if (meaning in unique_utterance_meanings and
                meaning in get_all_word_meanings(other_words, lexicon, "n") and
                not word.frozen):
                print("Removed meaning", meaning, "from word", word)
                word.remove_p_meaning(meaning)

def forget (lexicon):
    "adds a memory effect/garbage collection"
    unfrozen_words = [w for w in lexicon if not w.frozen]

    for w in unfrozen_words:
        print("Removing unfrozen word", w)
        lexicon.remove(w)

    return lexicon

def is_interpretation_correct (word, interpretation):
    print("received word", word, "interpretation", interpretation)
    if word.upper() == interpretation:
        return True
    return False

# reporting and statistics tools
def gen_statistics (run, words, scene, lexicon, forgetful_state, forget_after, interpretation):
    "generate experiment data for each run"
    converged = [w for w in lexicon if w.frozen]
    # this is a hack to get the ambiguity from Yu & Smith’s experiment, because
    # there is only one interpretation for each utterance (referential
    # uncertainty is zero), but 2 to 4 referents present, which is what they
    # call “ambiguity”; Trueswell always has “ambiguity” == 4
    if len(scene) == 1:
        ambiguity = len(scene[0])
    else:
        ambiguity = len(scene)
    stats = dict([("RUN", run + 1),
                  ("WORDS", words),
                  ("SCENE", scene),
                  ("LEXICON", lexicon),
                  ("CONVERGED", converged),
                  ("LEX_SIZE", len(lexicon)),
                  ("NO_CONVERGED", len(converged)),
                  ("FORGETFUL", forgetful_state),
                  ("FORGET_AFTER", forget_after),
                  ("AMBIGUITY", ambiguity),
                  ("INTERPRETATION", interpretation[0]),
                  ("INTERPRET_CORRECT", interpretation[1])])
    return stats

def output_statistics (f, data):
    "output experiment data to CSV file"
    with open(f + ".csv", "a") as csvfile:
        fields = ["RUN", "WORDS", "SCENE", "LEXICON", "CONVERGED",
                  "LEX_SIZE", "NO_CONVERGED", "FORGETFUL",
                  "FORGET_AFTER", "AMBIGUITY",
                  "INTERPRETATION", "INTERPRET_CORRECT"]
        writer = csv.DictWriter(csvfile, fieldnames=fields)
        # if file is empty, write header
        if os.stat(f + ".csv").st_size == 0:
            writer.writeheader()
        writer.writerows(data)

# Forced choice
# In order to compare to Stevens et al.’s results for Trueswell et al.’s
# experiment, we need to make our processor make a decision about which
# referent is going to be chosen as the correct referent for a word. I’ll
# explore two possibilities, one that takes inequivically from N(w) when
# possible, and another which assume some probability distribution. Note: this
# only works for Trueswell et al.’s experiment for now…
def interpret_word (pair, lexicon):
    "Makes a decision about the current word meaning"
    word = get_words(pair)
    word_n_meanings = get_all_word_meanings(word, lexicon, "n")
    word_p_meanings = get_all_word_meanings(word, lexicon, "p")
    utterance_meanings = get_all_utterance_meanings(get_meanings(pair))

    # does the word have meanings in N(w)? Choose from that necessarily.
    if word_n_meanings:
        # which of these meanings are avaiable in the utterance? Randomize from these.
        print("Interpreting from available necessary meanings", word_n_meanings)
        interpretation = random.sample(word_n_meanings.intersection(utterance_meanings), 1)[0]
        return (interpretation, is_interpretation_correct(word[0], interpretation))
    # if it doesn’t have any meanings in N(w), choose from P(w)
    else:
        print("Interpreting from available possible meanings", word_p_meanings)
        interpretation = random.sample(word_p_meanings.intersection(utterance_meanings), 1)[0]
        return (interpretation, is_interpretation_correct(word[0], interpretation))

# PROCESSOR
# Here we put everything together
# Remember: for each new word, add it to the lexicon and initiatize it with
# all available meanings (does Siskind do this, or does he apply some magic
# to consider the other words in the utterance? Like removing anything that
# must be necessarily contributed by some word and thus cannot be part of
# that new word’s meaning?
def siskind96_process (data,
                       stats_file = "",
                       stats = False,
                       forgetful = False,
                       forget_after = 0,
                       interpret = False):
    "learns a lexicon, Siskind (1996) style"
    lexicon = []
    statistics = []
    interpretation_log = []

    run = 0
    for pair in data:
        utterance_words = get_words(pair)
        utterance_meanings = get_meanings(pair)

        # reporting pt1
        print()
        print("-"*50)
        print("RUN", run, "\n")
        print("PROCESSING PAIR", pair)

        # purge unconsolidated meanings from memory
        # how similar is this to Siskind’s actual garbage collector?
        if forgetful and run % forget_after == 0:
            forget(lexicon)

        # add new words to lexicon and initialize them
        add_new_words(utterance_words, get_all_utterance_meanings(utterance_meanings), lexicon)

        # processing; rule1 is the only that actually changes anything
        # about the pair (by removing ref uncertainty), thus we need to
        # update pair
        pair = rule1(pair, lexicon)
        rule2(pair, lexicon)
        rule3(pair, lexicon)
        rule4(pair, lexicon)

        # reporting pt2
        print("Processed lexicon in run", run, "\n")
        print(view_lexicon(lexicon))

        # interpreting to compare with Steven et al.’s study
        interpretation = "NA" # not applicable by default
        if interpret:
            interpretation = interpret_word(pair, lexicon)
            print("Interpretation", interpretation)
            interpretation_log.append(interpretation)

        # aggregate statistics/reporting
        # gotta copy lexicon since
        current_lexicon = lexicon.copy()
        statistics.append(gen_statistics(run,
                                         utterance_words,
                                         utterance_meanings,
                                         current_lexicon,
                                         forgetful,
                                         forget_after,
                                         interpretation))

        # increment run count
        run += 1

    # generate statistics
    if stats:
        print("Writing statistics to", STATS_DIR + stats_file)
        output_statistics(STATS_DIR + stats_file, statistics)

    return lexicon

# Automating functions
def run_experiment (dataset, stats_file, stats = True, quiet = True,
                    forgetful = False, forget_after = 0, interpret = False):
    "run experiment n times automatically"
    # supress print output for faster runtime
    if quiet:
        sys.stdout = open(os.devnull, 'w')
    for data in dataset:
        siskind96_process(data, stats_file, stats, forgetful = forgetful,
                          forget_after = forget_after, interpret = interpret)
    # returns printing to normal
    if quiet:
        sys.stdout = sys.__stdout__

# Yu and Smith (2007), four experiments
def run_yu_smith_experiment_1 (times, ambiguity, stats_file = "yu1", stats = True,
                               quiet = True, forgetful = False, forget_after = 0):
    """
    Run Yu & Smith (2007) experiment 1
    Parameters:
    MAX_WORD_EXPOSURES = 6
    VOCABULARY_SIZE = 18
    AMBIGUITY = 2–4
    """
    vocabulary = vocab_generator.gen_vocabulary(yu_smith_2007.WORD_FILE, 18)
    max_word_exposures = 6
    dataset = []
    for i in range(times):
        dataset.append(yu_smith_2007.gen_experiment_data(ambiguity, vocabulary, max_word_exposures))

    run_experiment(dataset, stats_file, stats, quiet, forgetful, forget_after)


def run_yu_smith_experiment_2_1 (times, stats_file = "yu2_1", stats = True,
                                 quiet = True, forgetful = False, forget_after = 0):
    """
    Run Yu & Smith (2007) experiment 2.1
    Parameters:
    MAX_WORD_EXPOSURES = 8
    VOCABULARY_SIZE = 9
    AMBIGUITY = 4
    """
    vocabulary = vocab_generator.gen_vocabulary(yu_smith_2007.WORD_FILE, 9)
    max_word_exposures = 8
    ambiguity = 4
    dataset = []
    for i in range(times):
        dataset.append(yu_smith_2007.gen_experiment_data(ambiguity, vocabulary, max_word_exposures))

    run_experiment(dataset, stats_file, stats, quiet, forgetful, forget_after)

def run_yu_smith_experiment_2_2 (times, stats_file = "yu2_2", stats = True,
                                 quiet = True, forgetful = False, forget_after = 0):
    """
    Run Yu & Smith (2007) experiment 2.2
    Parameters:
    MAX_WORD_EXPOSURES = 12
    VOCABULARY_SIZE = 9
    AMBIGUITY = 4
    """
    vocabulary = vocab_generator.gen_vocabulary(yu_smith_2007.WORD_FILE, 9)
    max_word_exposures = 12
    ambiguity = 4
    dataset = []
    for i in range(times):
        dataset.append(yu_smith_2007.gen_experiment_data(ambiguity, vocabulary, max_word_exposures))


    run_experiment(dataset, stats_file, stats, quiet, forgetful, forget_after)

def run_yu_smith_experiment_2_3 (times, stats_file = "yu2_3", stats = True,
                                 quiet = True, forgetful = False, forget_after = 0):
    """
    Run Yu & Smith (2007) experiment 2.3
    Parameters:
    MAX_WORD_EXPOSURES = 6
    VOCABULARY_SIZE = 18
    AMBIGUITY = 4
    """
    vocabulary = vocab_generator.gen_vocabulary(yu_smith_2007.WORD_FILE, 18)
    max_word_exposures = 6
    ambiguity = 4
    dataset = []
    for i in range(times):
        dataset.append(yu_smith_2007.gen_experiment_data(ambiguity, vocabulary, max_word_exposures))

    run_experiment(dataset, stats_file, stats, quiet, forgetful, forget_after)


# Trueswell et al. (2013) experiment 1
def run_trueswell_et_al_experiment (times, stats_file = "trueswell",
                                    stats = True, quiet = True, forgetful = False,
                                    forget_after = 0, interpret = True):
    """
    Experimental parameters to simulate experiment 1 (from
    trueswell_et_al_2013.py):
    MAX_WORD_DISTRACTOR_PAIRING = 2 (or 40% coocurrence)
    NUMBER_OF_BLOCKS = 5 (* 12 words = 60 trials)
    VOCABULARY_SIZE = 12
    AMBIGUITY = 4 (+ the word = 5 referents per scene)
    """
    dataset = []
    for i in range(times):
        dataset.append(trueswell_et_al_2013.gen_experiment_data())

    run_experiment(dataset, stats_file, stats, quiet, forgetful, forget_after, interpret)

# Run all experiments and produce statistics
def run_all ():
    ambiguity = [2,3,4]
    times = 100

    # experiments without forgetting
    # all three ambiguity conditions in Yu & Smith, experiment 1
    for a in ambiguity:
        run_yu_smith_experiment_1(times, a)
    # experiment 2
    run_yu_smith_experiment_2_1(times)
    # experiment 3
    run_yu_smith_experiment_2_2(times)
    # experiment 4
    run_yu_smith_experiment_2_3(times)
    # Trueswell et al. experiment 1
    run_trueswell_et_al_experiment(times)

    # test forgetfulness; Trueswell is deadly affected by this, so won’t experiment
    forgetfullness = [2,5,7,8]
    for f in forgetfullness:
        for a in ambiguity:
            run_yu_smith_experiment_1(times, a, forgetful = True, forget_after = f)
        run_yu_smith_experiment_2_1(times, forgetful = True, forget_after = f)
        run_yu_smith_experiment_2_2(times, forgetful = True, forget_after = f)
        run_yu_smith_experiment_2_3(times, forgetful = True, forget_after = f)

# Extra stuff, crazy ideas

# EXAMPLE: a test run of Yu & Smith, exp 1:
# v = vocab_generator.gen_vocabulary(yu_smith_2007.WORD_FILE, 18)
# e = yu_smith_2007.gen_experiment_data(4, v, 6)
# run_experiment([e], "", stats = False, quiet = False, interpret = True)

# Trueswell et al. (2013) experiment 1
def run_random_trueswell (times, stats_file = "trueswell-random-order",
                          stats = True, quiet = True, forgetful = False,
                          forget_after = 0, interpret = True):
    """Trueswell et al. exp 1 reveals that order affects adapted Siskind. What
    if we randomized trial orders?
    """
    dataset = []
    for i in range(times):
        d = trueswell_et_al_2013.gen_experiment_data()
        random.shuffle(d)
        dataset.append(d)

    run_experiment(dataset, stats_file, stats, quiet, forgetful, forget_after, interpret)
