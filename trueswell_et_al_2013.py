#!/usr/bin/env python
# coding: utf-8
# trueswell_et_al_2013.py
# Implements Trueswell el al.’s (2013) first experiment

import random
from collections import Counter
import vocab_generator

# 5 images, 12 objects = 60 images
# (but we’re going to assume have only 12 objects)
# each image appears randomly with target word but only 40% of the time (twice)
# 5 blocks, same order of presentation

# Vocabulary
MAX_WORD_DISTRACTOR_PAIRING = 2
NUMBER_OF_BLOCKS = 5
WORD_FILE = "words/trueswell_stimuli.txt"
VOCABULARY = vocab_generator.gen_vocabulary(WORD_FILE, 12)
AMBIGUITY = 5

# cooccocurrence dictionary
# (how could I do this with a matrix?)
def init_cooccur_counter (vocabulary):
    dict = {}
    words = [w.sound for w in vocabulary]
    meanings = [w.semantics for w in vocabulary]

    for word in words:
        dict[word] = {}
        for meaning in meanings:
            dict[word][meaning] = 0

    return dict

# generate blocks
# the challenge here is never repeating the same index more than twice!
def gen_blocks (ambiguity,
                vocabulary = VOCABULARY,
                number_of_blocks = NUMBER_OF_BLOCKS,
                max_word_distractor_pairing = MAX_WORD_DISTRACTOR_PAIRING):
    """"
    returns list of Word objects; head is the word presented and tail is are
    the possible referents
    """
    blocks = []
    cooccurrences = init_cooccur_counter(vocabulary)

    # base order across blocks
    words = vocabulary.copy()
    random.shuffle(words)

    # now pair each word with more referents
    while len(blocks) < number_of_blocks:
        block = []
        for word in words:
            # a trial is a word plus some possible referents (distractors)
            # and including the word itself
            trial = []
            # begin by adding the word
            trial.append(word)

            # now add distractors, starting by the word
            trial.append(word)
            i = 0
            limit = 0
            while i < ambiguity - 1:
                distractor = vocabulary[random.randint(0, len(vocabulary) - 1)]
                # check to see if the distractor isn’t the current word or has
                # already cooccurred twice
                if (distractor not in trial and
                    cooccurrences[str(word)][str(distractor).upper()] < 2):
                    trial.append(distractor)

                    cooccurrences[str(word)][str(distractor).upper()] += 1
                    i += 1
                # hack to guarantee we don’t get into an infinite loop
                # sometimes, for some reason, the trial cannot be generated
                # because the conditions above are impossible to meet; in
                # those situations, return False I deal with this in
                # another function
                limit += 1
                if limit == 100:
                    return False

            # after generating trial, append to current block
            block.append(trial)

        # append block to blocks list
        blocks.append(block)

    return blocks

def gen_trial (block):
    "generate a list of words and distractors compatible with Siskind (1996)"
    # get word in head
    words = [block[0].sound]
    # get meanings in tail; each meaning in its separate list, which is
    # necessary for my implementation of Siskind since each is a possible
    # “utterance” interpretation for the word
    distractors = [[d.semantics] for d in block[1:]]

    # clean the block for reassembly
    block = []
    block.append(words)
    # add distractors to list tail
    for distractor in distractors:
        block.append(distractor)

    return block

def gen_experiment_data (ambiguity = AMBIGUITY):
    "outputs experiment for Siskind (1996)"
    blocks = gen_blocks(ambiguity)
    # hack: sometimes gen_blocks() will return False; while it does so, try
    # again until getting a valid block set
    while not blocks:
        blocks = gen_blocks(ambiguity)
    trials = []

    for block in blocks:
        for trial in block:
            trials.append(gen_trial(trial))

    return trials
